﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje2D : MonoBehaviour
{
    public float velocidad = 8;
    public float salto = 150;

    private Rigidbody2D personarg;
    // Start is called before the first frame update
    void Start()
    {
        personarg = GetComponent < Rigidbody2D > (); 
    }

    private void  FixedUpdate()
    {
        float movimientoH = Input.GetAxis("Horizontal");
        float movimientoV = Input.GetAxis("Vertical");
        Vector2 mov = new Vector2(movimientoH, movimientoV);
        personarg.AddForce(mov * velocidad);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            personarg.AddForce(new Vector2(0, salto));   
        }
        
    }
}
